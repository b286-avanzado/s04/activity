from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from .models import ToDoItem, Event
from .forms import LoginForm, AddTaskForm, RegisterForm, CreateEventForm

from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.forms.models import model_to_dict
from django.utils import timezone

def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
    event_list = Event.objects.filter(user_id = request.user.id)

    context = {
        'todoitem_list' : todoitem_list,
        'event_list' : event_list,
        "user" : request.user
    }
    # context = {'todoitem_list' : todoitem_list}

    return render(request, "todolist/index.html", context)

    # return HttpResponse("Hello from the views.py file")

def todoitem(request, todoitem_id):

    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    # response = "You are viewing the details of %s"
    # return HttpResponse(response %todoitem_id)
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def register(request):
    # users = User.objects.all()
    # is_user_registered = False
    # context = {
    #     "is_user_registered": is_user_registered
    # }

    # check if the username "johndoe" already exists
    # if it exists, change the value of variable is_user_registered to True
    # for indiv_user in users:
    #     if indiv_user.username == "johndoe":
    #         is_user_registered = True
    #         break

    # if is_user_registered == False:
    #     user = User()
    #     user.username = "johndoe"
    #     user.first_name = "John"
    #     user.last_name = "Doe"
    #     user.email = "john@mail.com"
    #     # The set_password is used to ensure that the password is hashed using Django's authentication framework
    #     user.set_password("john1234")
    #     user.is_staff = False
    #     user.is_active = True
    #     user.save()

    #     context = {
    #         "first_name": user.first_name,
    #         "last_name": user.last_name
    #     }

    # return render(request, "todolist/register.html", context)

    context = {}

    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid() == False:
            form = RegisterForm()
        else:
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            # password = make_password(form.cleaned_data['password'], salt=10, hasher='default')
            password = make_password(form.cleaned_data['password'])
            duplicate = User.objects.filter(username = username)
            duplicate = User.objects.filter(email = email)

            if not duplicate:
                User.objects.create(username = username, first_name = first_name, last_name = last_name, email = email, password = password)
                return redirect("todolist:login")
            else:
                context = {
                    "error" : True
                }

    return render(request, "todolist/register.html", context)
        
def change_password(request):
    is_user_authenticated = False

    user = authenticate(username="johndoe", password="johndoe1")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username='johndoe') # this is where we get the user with username 'johndoe'
        authenticated_user.set_password("johndoe2") # this is where we change the password
        authenticated_user.save()
        is_user_authenticated = True
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "todolist/change_password.html", context)

def login_view(request):

    context = {}

    if request.method == "POST":
        # Create a form instance and populate it with data from the request
        form = LoginForm(request.POST)
        # Check whether the data is valid
        # Runs validation routines for all the form fields and returns true and places the form's data in the cleaned_data attribute
        if form.is_valid() == False:
            # Return a blank login form
            form = LoginForm()
        else:
            # Receives the information from the form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username = username, password = password)
            context = {
                "username" : username,
                "password" : password
            }
            if user is not None:
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    "error" : True
                }

    return render(request, "todolist/login.html", context)


    # username = "johndoe"
    # password = "johndoe2"
    # user = authenticate(username = username, password = password)
    # context = {
    #     "is_user_authenticated" : False
    # }
    # print(user)
    # if user is not None:
    #     login(request, user)
    #     return redirect("todolist:index")
    # else:
    #     return render(request, "todolist/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("todolist:index")

def add_task(request):
    context = {}

    if request.method == "POST":
        form = AddTaskForm(request.POST)
        if form.is_valid() == False:
            form = AddTaskForm()
        else: 
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            duplicate = ToDoItem.objects.filter(task_name =  task_name)

            if not duplicate:
                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id = request.user.id)
                return redirect("todolist:index")

            else: 

                context = {
                    "error" : True
                }

    return render(request, "todolist/add_task.html", context)

def create_event(request):
    context = {}

    if request.method == "POST":
        form = CreateEventForm(request.POST)
        if form.is_valid() == False:
            form = CreateEventForm()
        else: 
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']
            event_date = form.cleaned_data['event_date']
            duplicate = Event.objects.filter(event_name = event_name)

            if not duplicate:
                Event.objects.create(event_name=event_name, description=description, event_date=event_date, user_id = request.user.id)
                return redirect("todolist:index")

            else: 

                context = {
                    "error" : True
                }

    return render(request, "todolist/create_event.html", context)

def view_event(request, event_id):

    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    event = get_object_or_404(Event, pk=event_id)
    # response = "You are viewing the details of %s"
    # return HttpResponse(response %todoitem_id)
    return render(request, "todolist/view_event.html", model_to_dict(event))