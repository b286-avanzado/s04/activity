from django.urls import path

from . import views

app_name = 'todolist'

urlpatterns = [
    # The path() function receives four arguments
    # We will focus on 2 arguments that are required: 'route' and 'view'
    path('', views.index, name="index"),

    # todolist/1
    # The <int:todoitem_id>/ allows for creating a dynamic link where the todoitem_id is provided
    path('task/<int:todoitem_id>/', views.todoitem, name="todoitem"),
    path('event/<int:event_id>/', views.view_event, name="view_event"),

    path('register', views.register, name="register"),
    path('change_password', views.change_password, name="change_password"),
    path('login', views.login_view, name="login"),
    path('logout', views.logout_view, name="logout"),
    path('add_task', views.add_task, name="add_task"),
    path('create_event', views.create_event, name="create_event"),

]